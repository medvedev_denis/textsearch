package ru.md.textSearch;

import java.io.*;

public class SearchFromDirectory {
    public static void main(String[] args) {
        try {
            BufferedReader in = new BufferedReader(new FileReader("settings.ini"));
            BufferedWriter transitionOut = new BufferedWriter(new FileWriter("Result.txt"));
            String catalogName = in.readLine();
            if (catalogName.substring(catalogName.length()-1).equals("\\")){
                catalogName = catalogName.substring(0,catalogName.length()-1);
            }
            System.out.println("Стартовый каталог: " + catalogName);
            String searchingText = in.readLine();
            System.out.println("Искомый текст: " + searchingText);
            ReaderDir readerDir = new ReaderDir(catalogName, searchingText, transitionOut);
            readerDir.start();
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}