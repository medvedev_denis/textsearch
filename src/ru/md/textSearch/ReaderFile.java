package ru.md.textSearch;

import java.io.*;
import java.util.ArrayList;

public class ReaderFile extends Thread {

    private String searchingText;
    private String pathOfFile;
    private String fileName;
    private BufferedWriter out;

    ReaderFile(String pathOfFile, String fileName, String textForSearch, BufferedWriter bw) {
        this.searchingText = textForSearch;
        this.pathOfFile = pathOfFile;
        this.fileName = fileName;
        this.out = bw;
    }

    @Override
    public void run() {
        ArrayList<Integer> stringValue = new ArrayList();
        String readString;
        try {
            BufferedReader in = new BufferedReader(new FileReader(pathOfFile + "\\" + fileName));
            int numberString = 1;
            while (true) {
                readString = in.readLine();
                if (readString != null) {
                    if (readString.contains(searchingText)){
                        stringValue.add(numberString);
                    }
                    numberString++;
                } else {
                    break;
                }
            }
            if (stringValue.size() != 0) {
                out.write("В файле " + fileName + " находящемся в каталоге \n\"" + pathOfFile +
                        "\"\nискомый текст \"" + searchingText + "\"" + " встречается в строках: " + stringValue + "\n\n");
                out.flush();
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}