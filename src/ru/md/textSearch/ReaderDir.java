package ru.md.textSearch;

import java.io.BufferedWriter;
import java.io.File;

public class ReaderDir extends Thread {

    private String pathOfFile;
    private String searchingText;
    private BufferedWriter transitionOut;

    ReaderDir(String pathOfFile, String textForSearch, BufferedWriter bw) {
        this.pathOfFile = pathOfFile;
        this.searchingText = textForSearch;
        this.transitionOut = bw;
    }

    @Override
    public void run() {
        File pathOfDirectory = new File(pathOfFile);
        File[] files = pathOfDirectory.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isFile()) {
                    if ((file.getName()).substring(file.getName().lastIndexOf(".") + 1).equals("txt")) {
                        ReaderFile readerFile = new ReaderFile(pathOfFile, file.getName(), searchingText, transitionOut);
                        readerFile.start();
                    }
                } else {
                    ReaderDir readerDir = new ReaderDir(pathOfFile + "\\" + file.getName(), searchingText, transitionOut);
                    readerDir.start();
                }
            }
        }
    }
}